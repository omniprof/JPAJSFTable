This example uses MySQL. In the Web Pages/SQL folder is an SQL script that must be run with administrative privilges once. Subsequently the tables will be recreated during the Arquillian tests with a standard user's credentials.

NetBeans reports when run on GlassFish:
Total time: 54.395s
Finished at: Thu Jan 05 21:51:53 EST 2017
Final Memory: 43M/333M

NetBeans reports when run on Payara 4.1
Total time: 19.365s
Finished at: Thu Jan 05 21:44:31 EST 2017
Final Memory: 44M/341M
